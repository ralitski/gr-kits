package goldrush.kit;

import com.gameon.libs.utils.ReflectionUtil;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import net.minecraft.server.v1_6_R3.EntityFishingHook;
import net.minecraft.server.v1_6_R3.MathHelper;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_6_R3.entity.CraftFish;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Fish;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.player.PlayerFishEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

/**
 *
 * @author ralitski
 */
public class KitItemListener implements Listener {
    
    private KitManager kits; //hmmm never used
    private List<Fish> hooks;
    
    public KitItemListener(KitManager kits) {
        this.kits = kits;
        this.hooks = new ArrayList<>();
    }
    
    @EventHandler
    public void onEntityDmg(EntityDamageByEntityEvent event) {
        if(event.getDamager() instanceof Player && event.getEntity() instanceof LivingEntity) {
            Player p = (Player)event.getDamager();
            LivingEntity le = (LivingEntity)event.getEntity();
            if(KitFactory.isCurser(p.getItemInHand())) {
                le.addPotionEffect(new PotionEffect(PotionEffectType.WITHER, 150, 0, true));
                if(le instanceof Player) {
                    Player p2 = (Player)le;
                    p2.sendMessage("You have been cursed by " + p.getName());
                }
            }
        }
    }
    
    //this doesn't work. it only gets called when the entity itself detects a collision.
    @EventHandler
    public void onProjectileHit(ProjectileHitEvent event) {
        //System.out.println(event + " " + event.getEventName() + " " + event.getEntity() + " " + event.getEntityType());
        LivingEntity le = event.getEntity().getShooter();
        if(le instanceof Player) {
            Player player = (Player)le;
            ItemStack is = player.getItemInHand();
            if(KitFactory.isGrapple(is)) {
                player.sendMessage("Grapple Hooked");
            }
        }
    }
    
    public void update() {
        //disabled; this borks stuff :[
//        /*
        for(int i = 0; i < this.hooks.size(); i++) {
            Fish fish = this.hooks.get(i);
            if(fish == null || fish.isDead() || !fish.isValid()) {
                this.hooks.remove(fish);
                continue;
            }
            EntityFishingHook hook = ((CraftFish)fish).getHandle();
            if (hook == null || !hook.isAlive()) {
                this.hooks.remove(fish);
                continue;
            }
            if (fish.isOnGround() || fish.getVelocity().length() < 0.001F) {
                //this doesn't get called
                this.doHook(fish);
                this.hooks.remove(fish);
            } else if (this.inBlock(hook, 0.2F)) {
                try {
                    Field f = ReflectionUtil.getField(hook.getClass(), "h");
                    f.setBoolean(hook, true);
                    fish.setVelocity(new Vector(0, 0, 0));
                    this.doHook(fish);
                    this.hooks.remove(fish);
                } catch (Exception e) {
                    //derp
                }
            } 
        }
//        */
    }
    
    private void doHook(Fish fish) {
        LivingEntity le = fish.getShooter();
        if (le instanceof Player) {
            Player player = (Player) le;
            player.sendMessage("Grapple Hooked");
        }
    }
    
    private boolean inBlock(EntityFishingHook hook, float big) {
        hook.width = hook.width + big;
        hook.length = hook.length + big;
        hook.height = hook.height + big;

        boolean ret = false;
        for (int i = 0; i < 8; ++i) {
            float f = ((float) ((i) % 2) - 0.5F) * hook.width * 0.8F;
            float f1 = ((float) ((i >> 1) % 2) - 0.5F) * 0.1F;
            float f2 = ((float) ((i >> 2) % 2) - 0.5F) * hook.width * 0.8F;
            int j = MathHelper.floor(hook.locX + (double) f);
            int k = MathHelper.floor(hook.locY + (double) hook.getHeadHeight() + (double) f1);
            int l = MathHelper.floor(hook.locZ + (double) f2);

            if (hook.world.u(j, k, l)) {
                ret = true;
            }
        }
        
        hook.width = hook.width - big;
        hook.length = hook.length - big;
        hook.height = hook.height - big;

        return ret;
    }
    
    @EventHandler
    public void onPlayerFish(PlayerFishEvent event) {
        ItemStack is = event.getPlayer().getItemInHand();
        if(KitFactory.isGrapple(is)) {
        switch(event.getState()) {
            case CAUGHT_ENTITY:
                //can pull entity to player, but doesn't
                //this.entityCatch(event.getPlayer(), event.getCaught());
                this.pullEntityTo(event.getPlayer(), event.getCaught().getLocation());
                break;
            case IN_GROUND:
                //pull player to place
                this.pullPlayerTo(event);
                break;
            case FAILED_ATTEMPT:
                //this.pullEntityTo(event.getPlayer(), event.getHook().getLocation(), 0.5D);
                //huehuehue this one's fun
                //maybe pull player to water? hmm...
                this.pullToFailed(event);
                break;
            case FISHING:
                this.boostHook(event);
                //when player throws hook
                break;
            default:
                break;
        }
        is.setDurability((short)0);
        } else if(KitFactory.isChain(is)) {
            switch (event.getState()) {
                case CAUGHT_ENTITY:
                    //can pull entity to player, but doesn't
                    //this.entityCatch(event.getPlayer(), event.getCaught());
                    this.pullEntityTo(event.getCaught(), event.getPlayer().getLocation());
                    break;
                case FISHING:
                    this.speedHook(event);
                    //when player throws hook
                    break;
                default:
                    break;
            }
            is.setDurability((short) 0);
        }
    }
    
    public void pullToFailed(PlayerFishEvent event) {
        if(event.getHook().isOnGround()) {
            this.pullEntityTo(event.getPlayer(), event.getHook().getLocation());
            this.hooks.remove(event.getHook());
            return;
        }
        Fish f = event.getHook();
        //removed - now checking for fish hook per tick (above)
        if(f instanceof CraftFish) {
            CraftFish fish = ((CraftFish)f);
            EntityFishingHook hook = fish.getHandle();
            //disabled; no work
            if(this.inBlock(hook, 1.0F))
                this.pullEntityTo(event.getPlayer(), fish.getLocation());
        }
    }
    
    public void boostHook(PlayerFishEvent event) {
        this.speedHook(event);
        Fish fish = event.getHook();
        try {
            EntityFishingHook hook = ((CraftFish)fish).getHandle();
            Method m = ReflectionUtil.getMethod(hook.getClass(), "a", Float.class, Float.class);
            float size = 0.1F;
            m.invoke(hook, size, size);
        } catch(Exception e) {
            //bork
        }
        this.hooks.add(fish);
    }
    
    public void speedHook(PlayerFishEvent event) {
        Fish fish = event.getHook();
        fish.setBounce(false);
        fish.setBiteChance(0);
        Vector vec = fish.getVelocity();
        //LivingEntity le = fish.getShooter();
        //vec.add(le.getVelocity());
        //vec.multiply(1.5D);
        vec.multiply(2.0D);
        fish.setVelocity(vec);
    }
    
    //pull entity to player huehuehue
    public void entityCatch(Player player, Entity e) {
        this.pullEntityTo(e, player.getLocation());
    }
    
    public void pullPlayerTo(PlayerFishEvent event) {
        Player player = event.getPlayer();
        Location location = event.getHook().getLocation();
        this.pullEntityTo(player, location);
        this.hooks.remove(event.getHook());
    }
    
    public void pullEntityTo(Entity e, Location loc) {
        this.pullEntityTo(e, loc, 1D);
    }
    
    public void pullEntityTo(Entity e, Location loc, double scale) {
        Location sub = loc.subtract(e.getLocation());
        Vector v = sub.toVector();
        //double mod = Math.min(4D, v.length());
        //if(v.length() < 1) mod *= 4D / v.length();
        //v.normalize();
        //v.multiply(mod);
        double y = Math.abs(v.getY());
        if(y != 0) {
            y = Math.log(y * 3) / 1.5D;
            //y = Math.min(1D/y, y) * 2D;
            //y = Math.sqrt(y) / 2D;
            //averaging it
            //y = y * y * v.getY();
            //y /= 5D;
            //y *= v.getY();
            
            /*
             * as you may be able to tell, I have no idea what I'm doing
             */
        }
        y += 0.5D;
        if(v.getY() < 0) y = -y;
        //if(e instanceof Player) ((Player)e).sendMessage(v.getY() + " > " + y);
        v.setY(y);
        v.multiply(0.7D * scale);
        double mod = 0.4D;
        v.setX(v.getX() * mod);
        v.setZ(v.getZ() * mod);
        e.setFallDistance(-5F);
        e.setVelocity(v);
    }
    
    private void normalize(Vector v) {
        double val = v.getY();
        double max = Math.sqrt(v.getX() * v.getX() + v.getZ() * v.getZ());
        max = Math.max(max, val/max);
        if(val > max) val = max;
        v.setY(val + 0.2D);
    }
    
    /*
     * player - person who right clicked
     * item - held item
     * entity - entity right clicked (if any)
     * block - block right clicked (if any)
     */
    public void onPlayerRightClick(Player player, ItemStack item, Entity entity, Block block) {
    }

    
}
