package goldrush.kit;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;

/**
 *
 * @author ralitski
 */
public class KitEffect extends KitBase {
    
    private List<PotionEffect> effects;
    
    public KitEffect(int id, Material icon, String name, double cost, PotionEffect...effects) {
        super(id, icon, name, cost);
        this.setEffects(effects);
    }
    
    public KitEffect setEffects(PotionEffect...effects) {
        this.effects = Arrays.asList(effects);
        return this;
    }

    @Override
    public boolean addTo(Player player) {
        return player.addPotionEffects(this.effects);
    }

}