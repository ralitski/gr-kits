package goldrush.kit;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ralitski
 */
public class IntWrapper {

    public int value;
    
    public IntWrapper(int i) {
        this.value = i;
    }
    
    public void set(int i) {
        this.value = i;
    }
    
    public int get() {
        return this.value;
    }
    
    public boolean is(int i) {
        return this.value == i;
    }
    
    public boolean is(IntWrapper wrap) {
        return this.value == wrap.value;
    }
    
    public static IntWrapper wrap(int i) {
        return new IntWrapper(i);
    }
    
    public static int unWrap(IntWrapper wrap) {
        return wrap.value;
    }
    
    public static List<IntWrapper> wrap(List<Integer> ints) {
        List<IntWrapper> ret = new ArrayList<>();
        for(Integer i : ints) {
            ret.add(wrap(i));
        }
        return ret;
    }
    
    public static List<Integer> unWrap(List<IntWrapper> wraps) {
        List<Integer> ret = new ArrayList<>();
        for(IntWrapper wrap : wraps) {
            ret.add(wrap.value);
        }
        return ret;
    }
}
