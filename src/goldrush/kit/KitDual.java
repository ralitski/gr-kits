package goldrush.kit;

import java.util.Arrays;
import java.util.List;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;

/**
 * a kit with effects and items.
 * 
 * @author ralitski
 */
public class KitDual extends KitBase {
    
    private ItemStack[] items;
    private List<PotionEffect> effects;
    
    public KitDual(int id, Material icon, String name, double cost) {
        super(id, icon, name, cost);
    }
    
    public KitDual addItems(ItemStack...items) {
        this.items = items;
        return this;
    }
    
    public KitDual addEffects(PotionEffect...effects) {
        this.effects = Arrays.asList(effects);
        return this;
    }

    @Override
    public boolean addTo(Player player) {
        return (this.items != null ? player.getInventory().addItem(this.items).isEmpty() : true) && (this.effects != null ? player.addPotionEffects(this.effects) : true);
    }

}
