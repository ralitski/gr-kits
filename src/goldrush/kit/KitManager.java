package goldrush.kit;

import com.gameon.libs.gems.GemHandler;
//import goldrush.GoldRushPlugin;
//import goldrush.main.OutputAssembler;
import java.util.*;
import org.bukkit.*;
import org.bukkit.configuration.Configuration;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.craftbukkit.v1_6_R3.inventory.CraftInventoryCustom;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.plugin.Plugin;

/**
 *
 * @author ralitski
 */
public class KitManager {

    public List<Kit> kits;
    public KitInventory kitInv;
    public CraftInventoryCustom inv;
    //TODO convert this to corelib MapList
    public Map<String, List<IntWrapper>> playerKits;
    public KitItemListener kitItems;
    
    public static EntityType seller;
    
    static {
        seller = EntityType.VILLAGER;
    }
    
    private Plugin user;
    
    public KitManager() {
        user = null; //GoldRushPlugin.getInstance()
        this.kitInv = new KitInventory(this);
        this.playerKits = new HashMap<>();
        this.kitItems = new KitItemListener(this);
        Bukkit.getPluginManager().registerEvents(this.kitItems, user);
        this.addKits();
    }
    
    public void addKit(Kit kit) {
        this.kits.add(kit);
    }
    
    public void openKitInventory(Player player) {
        player.openInventory(this.inv);
        //CraftInventoryView view = new CraftInventoryView(player, this.inv, new CraftContainer(this.inv, player, 0));
        //player.openInventory(view);
    }
    
    public Inventory getKitInventory() {
        return this.inv;
    }
    
    public ItemStack[] assemble() {
        ItemStack[] items = new ItemStack[this.kits.size()];
        for(int i = 0; i < this.kits.size(); i++) {
            items[i] = this.assembleItem(this.kits.get(i));
        }
        return items;
    }
    
    //assembles item for the kit to be placed in inv GUI
    private ItemStack assembleItem(Kit kit) {
        ItemStack item = new ItemStack(kit.icon());
        
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(kit.name());
        List<String> data = new ArrayList<>();
        data.add(ChatColor.GRAY + ChatColor.stripColor("Cost: " + kit.cost() + " gems"));
        data.addAll(kit.info());
        meta.setLore(data);
        
        item.setItemMeta(meta);
        
        return item;
    }
    
    public void refresh() {
        int size = this.kits.size();
        if(size % 9 == 0);
        else size = size - (size % 9) + 9;
        this.inv = new CraftInventoryCustom(this.kitInv, size, "Kits");
        this.inv.clear();
        this.inv.addItem(this.assemble());
    }
    
    public void createSeller(Player player) {
        World world = player.getWorld();
        Location location = player.getLocation();
        Entity entity = world.spawnEntity(location, seller);
        if(!(entity instanceof LivingEntity)) return;
        LivingEntity le = (LivingEntity)entity;
        le.setMetadata("kit", new FixedMetadataValue(user, true));
        le.setCustomName("Kit Salesman");
        le.setCustomNameVisible(true);
    }
    
    public boolean isSeller(Entity e) {
        if(!(e instanceof LivingEntity)) return false;
        LivingEntity le = (LivingEntity)e;
        List<MetadataValue> metas = le.getMetadata("kit");
        if(metas == null) return false;
        for(MetadataValue meta : metas) {
            if(meta.asBoolean() == true) return true;
        }
        //metadata doesn't stay after server shutdown :(
        return "Kit Salesman".equals(le.getCustomName());
    }
    
    //tries to add the kit to the player
    public void addKitTo(Player player, ItemStack item) {
        if(player == null || item == null)
            return;
        ItemMeta meta = item.getItemMeta();
        if(meta == null)
            return;
        String name = meta.getDisplayName();
        Kit kit = null;
        double balance = 0;
        try {
            balance = GemHandler.getBalance(player);
        } catch(Exception e) {}
        for(Kit kitt : this.kits) {
            if(kitt.name().equals(name) && (balance >= kitt.cost()))
                kit = kitt;
        }
        if(kit == null) {
            player.sendMessage("You were unable to purchase the kit: " + kit.name());
            return;
        } else
            player.sendMessage("You have purchased the kit: " + kit.name());
        try {
            GemHandler.removeGems(player, kit.cost());
        } catch(Exception e) {}
        List<IntWrapper> kitList = this.playerKits.get(player.getName());
        if(kitList == null) kitList = new ArrayList<>();
        kitList.add(new IntWrapper(kit.getId()));
        this.playerKits.put(player.getName(), kitList); //if it had to be newly instantiated, gotta add it
    }
    
    public void activateKits(Player player) {
        List<IntWrapper> kitList = this.playerKits.get(player.getName());
        if(kitList == null || kitList.isEmpty());
        else
            for(Kit kit : this.kits) {
                boolean used = false; //make sure a kit only is activated once
                for(int i = 0; i < kitList.size(); i++) {
                    IntWrapper wrap = kitList.get(i);
                    if(!used && wrap.is(kit.getId())) {
                        kitList.remove(wrap);
                        kit.addTo(player);
                        player.sendMessage("Activated kit: " + kit.name());
                        used = true;
                    }
                }
            }
    }
    
    public List<Kit> getKits(Player player) {
        List<Kit> ret = new ArrayList<>();
        List<IntWrapper> kitList = this.playerKits.get(player.getName());
        if(kitList == null || kitList.isEmpty()) return ret;
        else
            for(Kit kit : this.kits) {
                for(IntWrapper wrap : kitList) {
                    if(wrap.is(kit.getId())) {
                        ret.add(kit);
                        continue;
                    }
                }
            }
        return ret;
    }
    
    public void activateKit(Player player, Kit kit) {
        kit.addTo(player);
    }
    
    public void loadKits(Configuration config) {
        ConfigurationSection sect = config.getConfigurationSection("kits");
        if(sect == null) sect = config.createSection("kits");
        Set<String> props = sect.getKeys(false);
        for(String prop : props) {
            this.playerKits.put(prop, IntWrapper.wrap(sect.getIntegerList(prop)));
        }
    }
    
    public void saveKits(Configuration config) {
        config.createSection("kits");
        ConfigurationSection sect = config.getConfigurationSection("kits");
        for(String player : this.playerKits.keySet()) {
            List<IntWrapper> kitList = this.playerKits.get(player);
            if(kitList == null || kitList.isEmpty()) continue;
            sect.set(player, IntWrapper.unWrap(this.playerKits.get(player)));
        }
    }
    
    /*
    public void loadKits(FileManager file) {
        file.load("kits");
        for(String prop : file.getProperties()) {
            Player player = Bukkit.getPlayerExact(prop);
            String data = file.getProperty(prop);
            if(data.isEmpty()) continue;
            String[] dataa = data.split(",");
            List<Integer> kitList = new ArrayList<>();
            for(String s : dataa) {
                kitList.add(Integer.parseInt(s));
            }
            this.playerKits.put(player, kitList);
        }
    }
    
    public void saveKits(FileManager file) {
        file.load("kits");
        //clearing previous properties to get rid of users with no kits
        for(String prop : file.getProperties())
            file.removeProperty(prop);
        for(Player player : this.playerKits.keySet()) {
            List<Integer> kitList = this.playerKits.get(player);
            if(kitList == null || kitList.isEmpty()) continue;
            String data = "";
            for(int i : kitList) {
                data = data.concat("," + i);
            }
            data.replaceFirst(",", "");
            file.addProperty(player.getName(), data);
        }
    }
    */
    
    /*
     * kits:
     * escapist: speed, low cost
     * miner: pickaxe
     */
    
    public void addKits() {
        this.kits = new ArrayList<>();
        KitFactory.addKits(this.kits);
        this.refresh();
    }
}
