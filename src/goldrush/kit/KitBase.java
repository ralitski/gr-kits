package goldrush.kit;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;

/**
 *
 * @author ralitski
 */
public abstract class KitBase implements Kit {
    
    //ID used to identify the kits a player has bought
    //stored with <Player, List<Integer>> map to add kits to player on game start
    private int id;
    private Material icon;
    private String name;
    private double cost;
    private List<String> info;
    
    public KitBase(int id, Material icon, String name, double cost) {
        this.id = id;
        this.icon = icon;
        this.name = name;
        this.cost = cost;
        this.setInfo();
    }
    
    @Override
    public Kit setInfo(String...info) {
        for(int i = 0; i < info.length; i++) {
            info[i] = ChatColor.GRAY + ChatColor.stripColor(info[i]);
        }
        this.info = Arrays.asList(info);
        return this;
    }

    @Override
    public Material icon() {
        return this.icon;
    }

    @Override
    public String name() {
        return ChatColor.WHITE + this.name;
    }

    @Override
    public double cost() {
        //return 0;
        return this.cost;
    }
    
    public List<String> info() {
        return this.info;
    }
    
    public int getId() {
        return this.id;
    }
    
    public void setId(int id) {
        this.id = id;
    }

}
