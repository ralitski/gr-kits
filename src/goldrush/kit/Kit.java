/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package goldrush.kit;

import java.util.List;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 *
 * @author ralitski
 */
public interface Kit {
    
    /*
     * id's are used to map kits bought to players, so must be unique;
     * names are used to map items to kits, so must also be unique.
     */
    
    public int getId();
    
    public void setId(int id);
    
    public boolean addTo(Player player);
    
    public Material icon();
    
    public String name();
    
    public double cost();
    
    public Kit setInfo(String...info);
    
    public List<String> info();
}
