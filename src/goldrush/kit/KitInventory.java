package goldrush.kit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import org.bukkit.Material;
import org.bukkit.entity.HumanEntity;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;

/**
 *
 * @author ralitski
 */
public class KitInventory implements InventoryHolder {
    
    public KitManager manager;
    
    public KitInventory(KitManager manager) {
        this.manager = manager;
    }

    @Override
    public Inventory getInventory() {
        return this.manager.inv;
    }
    
}
