package goldrush.kit;

import com.gameon.libs.items.CustomItemData;
//import goldrush.GoldRushPlugin;
import java.util.List;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_6_R3.inventory.CraftItemStack;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

/**
 *
 * @author ralitski
 */
public class KitFactory {
    
    private static int GAME_TICKS;
    
    //special items
    public static ItemStack grapple;
    public static ItemStack chain;
    public static ItemStack curser; //not cursor
    
    public static ItemStack[] getSpecialItems() {
        return new ItemStack[]{grapple.clone(), chain.clone(), curser.clone()};
    }
    
    static {
        GAME_TICKS = 36000; //number of ticks in a game
        
        //special items
        grapple = CraftItemStack.asCraftCopy(nameItem(Material.FISHING_ROD, ChatColor.GOLD + "Grapple")); //"hooker" lel
        chain = CraftItemStack.asCraftCopy(nameItem(Material.FISHING_ROD, ChatColor.GOLD + "Chain"));
        curser = CraftItemStack.asCraftCopy(nameItem(Material.IRON_SWORD, ChatColor.DARK_GRAY + "Wither Blade"));
        CustomItemData.setBoolean(grapple, "isGrapple", true);
        CustomItemData.setBoolean(chain, "isChain", true);
        CustomItemData.setBoolean(curser, "isCurser", true);
    }
    
    public static boolean isGrapple(ItemStack is) {
        ItemMeta meta = is.getItemMeta();
        return is.getType() == Material.FISHING_ROD && meta.hasDisplayName() && meta.getDisplayName().equals(ChatColor.GOLD + "Grapple");
        //return CustomItemData.getBoolean(is, "isGrapple");
    }
    
    public static boolean isChain(ItemStack is) {
        ItemMeta meta = is.getItemMeta();
        return is.getType() == Material.FISHING_ROD && meta.hasDisplayName() && meta.getDisplayName().equals(ChatColor.GOLD + "Chain");
        //return CustomItemData.getBoolean(is, "isChain");
    }
    
    public static boolean isCurser(ItemStack is) {
        ItemMeta meta = is.getItemMeta();
        return is.getType() == Material.IRON_SWORD && meta.hasDisplayName() && meta.getDisplayName().equals(ChatColor.DARK_GRAY + "Wither Blade");
        //return CustomItemData.getBoolean(is, "isCurser");
    }

    public static void addKits(List<Kit> kits) {
        
        kits.add(new KitDual(0, Material.IRON_SWORD, "Warrior", 10.0D)
                .addItems(new ItemStack(Material.IRON_SWORD), new ItemStack(Material.BOW),
                new ItemStack(Material.ARROW, 16),
                new ItemStack(Material.POTION, 1, (short)8201))
                //.addEffects(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, GAME_TICKS, 0, true))
                .setInfo("Iron Sword x1","Bow x1","Arrow x16","Strength Potion (3:00) x1"));
        
        kits.add(new KitDual(1, Material.CAKE, "Glutton", 7.0D)
                .addItems(new ItemStack(Material.CAKE),
                new ItemStack(Material.COOKED_BEEF, 2),
                new ItemStack(Material.COOKIE, 4))
                .addEffects(new PotionEffect(PotionEffectType.SATURATION, GAME_TICKS, 0, false))
                .setInfo("Cake x1","Steak x2","Cookie x4","Saturation (30:00)"));
        
        kits.add(new KitItem(2, Material.DIAMOND_AXE, "Lumberjack", 10.0D,
                new ItemStack(Material.DIAMOND_AXE),
                new ItemStack(Material.LOG, 32))
                .setInfo("Diamond Axe x1","Logs x32"));
        
        kits.add(new KitItem(3, Material.FISHING_ROD, "Grappler", 12.0D,
                grapple)
                .setInfo("Grappling Hook x1"));
        
        kits.add(new KitItem(4, Material.FISHING_ROD, "Chain Thrower", 8.0D,
                chain)
                .setInfo("Chain Whip x1"));
        
        kits.add(new KitItem(5, Material.IRON_PICKAXE, "Quickstart", 14.0D,
                new ItemStack(Material.IRON_PICKAXE),
                new ItemStack(Material.LOG, 16),
                new ItemStack(Material.COBBLESTONE, 32))
                .setInfo("Iron Pickaxe x1", "Logs x16", "Cobblestone x32"));
        
        kits.add(new KitItem(6, Material.SULPHUR, "Demolition", 8.0D,
                new ItemStack(Material.TNT, 12),
                new ItemStack(Material.STONE_PLATE, 4),
                new ItemStack(Material.FLINT_AND_STEEL, 1))
                .setInfo("TNT x4", "Pressure Plate x4", "Flint and Steel x1"));
        
        kits.add(new KitEffect(7, Material.GOLDEN_APPLE, "Health Boost", 8.0D,
                new PotionEffect(PotionEffectType.HEALTH_BOOST, GAME_TICKS, 2, true))
                .setInfo("+12 max health"));
        
        kits.add(new KitItem(8, Material.NETHER_STAR, "Wither Curse", 6.0D,
                curser)
                .setInfo("Wither Blade x1"));
    }
    
    //yay helpers
    
    public static ItemStack nameItem(Material mat, String name) {
        return nameItem(mat, 1, name);
    }
    
    public static ItemStack nameItem(Material mat, int count, String name) {
        return nameItem(new ItemStack(mat, count), name);
    }
    
    public static ItemStack nameItem(ItemStack item, String name) {
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(name);
        item.setItemMeta(meta);
        return item;
    }
}
