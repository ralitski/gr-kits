package goldrush.kit;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 *
 * @author ralitski
 */
public class KitItem extends KitBase {
    
    private ItemStack[] items;
    
    public KitItem(int id, Material icon, String name, double cost, ItemStack...items) {
        super(id, icon, name, cost);
        this.items = items;
    }

    @Override
    public boolean addTo(Player player) {
        return player.getInventory().addItem(this.items).isEmpty();
    }
}
